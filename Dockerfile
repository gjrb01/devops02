FROM node

LABEL maintainer="Gilmar Batista <gjrb@cesar.school>"

WORKDIR /app

COPY ./App/app.js /app

RUN npm install express

ENTRYPOINT ["node", "app.js"]